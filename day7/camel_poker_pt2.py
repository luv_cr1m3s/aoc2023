#!/usr/bin/env python3

# broken needs refactor

from functools import cmp_to_key


def get_input(filename):
    result = ""

    with open(filename, "r") as file:
        result = file.read()

    return result


LABELS = {"2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7,
          "8": 8, "9": 9, "T": 10, "J": 1, "Q": 12, "K": 13, "A": 14}


def parse_plays(plays):
    result = {}
    plays = plays.split("\n")
    plays = [i for i in plays if len(i) > 0]

    for i in plays:
        hand, bid = i.split(" ")
        result[hand] = int(bid)

    return result


def sep_hand_type(hands):
    types = ['high', 'one_p', 'two_p', 'three', 'house', 'four', 'five']
    result = {"five": [], "four": [], "house": [],
              "three": [], "two_p": [], "one_p": [], "high": []}

    for h in hands:
        index = 0
        if uniq_h := len(set(h)):
            jokers = h.count("J")
            jokers_count = h.count("J")

            letters_d = {}
            for c in h:
                if c != "J":
                    if c in letters_d.keys():
                        letters_d[c] += 1
                    else:
                        letters_d[c] = 1
            if jokers:
                uniq_h -= 1
            letters = letters_d.values()
            letters = sorted(letters)
            letters = letters[::-1]

            if uniq_h == 1 or uniq_h == 0:
                index = 6
            if uniq_h == 2:
                if letters[0] + jokers_count == 4:
                    index = 5
                elif letters[0] + jokers_count == 3 and letters[1] == 2:
                    index = 4
                else:
                    index = 3
            if uniq_h == 3:
                if letters[0] + jokers_count == 3 and letters[1] == 2:
                    index = 4
                elif letters[0] + jokers_count == 3:
                    index = 3
                else:
                    index = 2
            if uniq_h == 4:
                index = 1
            if uniq_h == 5:
                index = 0
        print(f"{h}, {letters_d.items()}, {types[index]}, {jokers}, {jokers_count}")
        result[types[index]].append(h)

    return result


def compare_labels(a, b):
    for (i, j) in zip(a, b):
        # print(f"{i}: {LABELS[i]}, {j}: {LABELS[j]}, {LABELS[i] - LABELS[j]}")
        if LABELS[i] < LABELS[j]:
            return -1
        if LABELS[j] < LABELS[i]:
            return 1
    return 0


def sort_types(hands):
    for k, v in hands.items():
        v[:] = sorted(v, key=cmp_to_key(compare_labels))


if __name__ == '__main__':
    a = get_input("first_star.txt")
    # a = get_input("test.txt")
    plays = parse_plays(a)
    sep_plays = sep_hand_type(plays)

    sort_types(sep_plays)
    result = []
    types = ["five", "four", "house", "three", "two_p", "one_p", "high"]
    types = types[::-1]

    for t in types:
        if len(sep_plays[t]) > 0:
            result += sep_plays[t]
    hand_sum = 0
    for i, v in enumerate(result):
        tmp = (i + 1) * plays[v]
        hand_sum += tmp

    print(hand_sum)
