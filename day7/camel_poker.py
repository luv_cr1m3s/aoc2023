#!/usr/bin/env python3

from functools import cmp_to_key


def get_input(filename):
    result = ""

    with open(filename, "r") as file:
        result = file.read()

    return result


LABELS = {"2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7,
          "8": 8, "9": 9, "T": 10, "J": 11, "Q": 12, "K": 13, "A": 14}


def parse_plays(plays):
    result = {}
    plays = plays.split("\n")
    plays = [i for i in plays if len(i) > 0]

    for i in plays:
        hand, bid = i.split(" ")
        result[hand] = int(bid)

    return result


def parse_hand_type(hands):
    result = {"five": [], "four": [], "house": [],
              "three": [], "two_p": [], "one_p": [], "high": []}
    for h in hands:
        if uniq_h := len(set(h)):
            if uniq_h == 1:
                result["five"].append(h)
            elif uniq_h == 2:
                if h.count(h[0]) == 1 or h.count(h[0]) == 4:
                    result["four"].append(h)
                else:
                    result["house"].append(h)
            elif uniq_h == 3:
                cards = list(set(h))
                for card in cards:
                    if h.count(card) == 3:
                        result["three"].append(h)
                        break
                else:
                    result["two_p"].append(h)
            elif uniq_h == 4:
                result["one_p"].append(h)
            else:
                result["high"].append(h)
    return result


def compare_labels(a, b):
    for (i, j) in zip(a, b):
        # print(f"{i}: {LABELS[i]}, {j}: {LABELS[j]}, {LABELS[i] - LABELS[j]}")
        if LABELS[i] < LABELS[j]:
            return -1
        if LABELS[j] < LABELS[i]:
            return 1
    return 0


def sort_types(hands):
    for k, v in hands.items():
        v[:] = sorted(v, key=cmp_to_key(compare_labels))
        print(v)


if __name__ == '__main__':
    a = get_input("first_star.txt")
    # a = get_input("test.txt")
    plays = parse_plays(a)
    sep_plays = parse_hand_type(plays)

    for k in sep_plays.keys():
        print(f"{k}\n{sep_plays[k]}")

    sort_types(sep_plays)
    result = []
    types = ["five", "four", "house", "three", "two_p", "one_p", "high"]
    types = types[::-1]

    for t in types:
        if len(sep_plays[t]) > 0:
            result += sep_plays[t]
    # result = result[::-1]
    # print(result)
    hand_sum = 0
    for i, v in enumerate(result):
        # print(plays[v])
        tmp = (i + 1) * plays[v]
        # print(tmp)
        hand_sum += tmp

    print(hand_sum)
