#!/usr/bin/env python3

import re


def get_input(filename):
    result = ""

    with open(filename, "r") as file:
        result = file.read()

    return result


def engine_read(input):
    input_array = input.split("\n")
    input_array = [i for i in input_array if len(i) > 0]
    input_array = [i.strip() for i in input_array]
    nums = []
    special_symbols = {}

    for i in range(len(input_array)):
        tmp = []
        for j in range(len(input_array[i])):
            if input_array[i][j].isdigit():
                tmp.append([i, j])
            else:
                if len(tmp) > 0:
                    nums.append(tmp)
                    tmp = []
                if input_array[i][j] != ".":
                    special_symbols[(i, j,)] = input_array[i][j]
        if len(tmp) > 0:
            nums.append(tmp)

    return nums, special_symbols, input_array


def generate_field(dot):
    return [(dot[0] - 1, dot[1] + 1,),
            (dot[0], dot[1] + 1,),
            (dot[0] + 1, dot[1] + 1,),
            (dot[0] - 1, dot[1],),
            (dot[0] + 1, dot[1],),
            (dot[0] - 1, dot[1] - 1,),
            (dot[0], dot[1] - 1,),
            (dot[0] + 1, dot[1] - 1,)]


def engine_count(nums, symbols, input_array):
    result = 0
    tmp = []
    tmp_nums = []

    for num in nums:
        add_flag = 0
        for digit in num:
            dot_field = generate_field(digit)
            for point in dot_field:
                if point in symbols.keys():
                    add_flag = 1
        if add_flag == 1:
            val = ""
            for digit in num:
                val += input_array[digit[0]][digit[1]]
            tmp.append(val)
            tmp_nums.append(num)
            result += int(val)

    return result, tmp, tmp_nums


if __name__ == '__main__':
    a = """
        467..114..
        ...*......
        ..35..633.
        ......#...
        617*......
        .....+.58.
        ..592.....
        ......755.
        ...$.*....
        .664.598..
        """
    b = get_input("first_star")
    # print(b)
    nums, symbols, input_array = engine_read(b)
    res, tmp, tmp_nums = engine_count(nums, symbols, input_array)
    flast_arr = [i for j in tmp_nums for i in j]

    for i in range(len(input_array)):
        for j in range(len(input_array[i])):
            if [i, j] in flast_arr:
                print('\033[92m' + input_array[i][j] + '\033[0m', end="")
            elif (i, j,) in symbols.keys():
                print('\033[94m' + input_array[i][j] + '\033[0m', end="")
            else:
                print(input_array[i][j], end="")
        print()

    print(res)
