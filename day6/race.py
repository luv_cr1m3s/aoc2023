#!/usr/bin/env python3


def get_input(filename):
    result = ""

    with open(filename, "r") as file:
        result = file.read()

    return result


def count_winable(races):
    input_array = races.split("\n")
    input_array = [i for i in input_array if len(i) > 0]
    print(input_array)
    time = input_array[0].split(":")
    distance = input_array[1].split(":")
    time_arr = [int(i) for i in time[1].split(" ") if i.isdigit()]
    dist_arr = [int(i) for i in distance[1].split(" ") if i.isdigit()]

    print(time_arr)
    print(dist_arr)

    result = []

    for (t, d)  in zip(time_arr, dist_arr):
        race_wins = 0
        for i in range(t):
            speed = i
            dist = speed * (t - i)
            print(f"distance: {dist}")
            if dist > d:
                race_wins += 1
        print(f"Race wins number: {race_wins}")
        result.append(race_wins)
    
    return result

if __name__ == '__main__':
    a = get_input("first_star.txt")
    #a = get_input("test.txt")
    #print(b)
    wins = count_winable(a)
    print(wins)
    wins_mul = 1
    for i in wins:
        wins_mul *= i

    print(wins_mul)
