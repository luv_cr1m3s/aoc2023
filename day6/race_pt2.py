#!/usr/bin/env python3


def get_input(filename):
    result = ""

    with open(filename, "r") as file:
        result = file.read()

    return result


def count_winable(races):
    input_array = races.split("\n")
    input_array = [i for i in input_array if len(i) > 0]
    print(input_array)
    time = input_array[0].split(":")
    distance = input_array[1].split(":")
    result = 0
    time_arr = [i for i in time[1].split(" ") if i.isdigit()]
    dist_arr = [i for i in distance[1].split(" ") if i.isdigit()]
    time_join = int("".join(time_arr))
    dist_join = int("".join(dist_arr))

    result = 0
    mid = time_join // 2
    start_win = 0

    for i in range(mid + 1):
        speed = i
        dist = speed * (time_join - i)
        if dist >= dist_join:
            start_win = i
            break
    # range of numbers that can't win 0..start_win - 1
    missed_numbers = start_win - 1
    # to exlude double mid used -1
    result = time_join - missed_numbers * 2 - 1

    return result

if __name__ == '__main__':
    a = get_input("first_star.txt")
    #a = get_input("test.txt")
    #print(b)
    wins = count_winable(a)
    print(wins)