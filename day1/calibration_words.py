#!/usr/bin/env python3

import regex

def get_input(filename):
    result = ""

    with open(filename, "r") as file:
        result = file.read()

    return result


DIGITS = {
    "1": 1,
    "one": 1,
    "2": 2,
    "two": 2,
    "3": 3,
    "three": 3,
    "4": 4,
    "four": 4,
    "5": 5,
    "five": 5,
    "6": 6,
    "six": 6,
    "7": 7,
    "seven": 7,
    "8": 8,
    "eight": 8,
    "9": 9,
    "nine": 9
}


def calibrate(input):
    input_array = input.split("\n")
    input_array = [i for i in input_array if len(i) > 0]

    result = 0

    for line in input_array:
        if digits := regex.findall("([0-9]|one|two|three|four|five|six|seven|eight|nine)", line, overlapped=True):
            first, last = DIGITS[digits[0]], DIGITS[digits[-1]]
            print(line)
            print(first, last)
            result += first * 10 + last

    return result


if __name__ == '__main__':
    b = get_input("first_star")
    print(b)
    print(calibrate(b))
