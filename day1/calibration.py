#!/usr/bin/env python3

import re


def get_input(filename):
    result = ""

    with open(filename, "r") as file:
        result = file.read()

    return result


def calibrate(input):
    input_array = input.split("\n")
    input_array = [i for i in input_array if len(i) > 0]

    result = 0

    for line in input_array:
        if first_digit := re.search("[0-9]", line):
            last_digit = re.search("[0-9]", line[::-1])

            print(first_digit.group(), last_digit.group())
            result += int(first_digit.group() + last_digit.group())

    return result


if __name__ == '__main__':
    b = get_input("first_star")
    print(b)
    print(calibrate(b))
