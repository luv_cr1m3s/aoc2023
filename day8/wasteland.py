#!/usr/bin/env python3


def get_input(filename):
    result = ""

    with open(filename, "r") as file:
        result = file.read()

    return result

def parse_input(str):
    pattern, directions = str.split("\n\n")

    directions = directions.split("\n")
    directions_dict = {}
    for d in directions:
        name, conn = d.split(" = ")
        a, b= conn[1:-1].split(", ")
        directions_dict[name]  = [a, b]

    print(pattern, directions_dict.items())
    return pattern, directions_dict


def walk(pattern, data):
    count = 0
    current = "AAA" 

    while current != "ZZZ":
        count += 1
        current = data[current][0 if pattern[0] == "L" else 1]
        pattern = pattern[1:] + pattern[0]
    
    print(count) 


if __name__ == '__main__':
    a = get_input("first_star.txt")
    #a = get_input("test2.txt")
    print(a)
    pattern, data = parse_input(a)
    walk(pattern, data)