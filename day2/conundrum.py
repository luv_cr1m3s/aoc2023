#!/usr/bin/env python3


def get_input(filename):
    result = ""

    with open(filename, "r") as file:
        result = file.read()

    return result


def conundum(input):
    input_array = input.split("\n")
    input_array = [i for i in input_array if len(i) > 0]
    result = {}

    for line in input_array:
        game = line.split(":")
        game_id = game[0].split(" ")[1]
        game_id = int(game_id)
        result[game_id] = {"red": [], "green": [], "blue": []}

        for game_set in game[1].split(";"):
            for cube in game_set.split(","):
                cube = cube.strip()
                count, color = cube.split(" ")
                count = int(count)
                result[game_id][color].append(count)

    return result


def count_games(input):
    result = 0

    for k, v in input.items():
        if max(v["red"]) > 12:
            continue
        if max(v["green"]) > 13:
            continue
        if max(v["blue"]) > 14:
            continue
        result += k

    return result


if __name__ == '__main__':
    b = get_input("first_star")
    data = conundum(b)
    print(count_games(data))
