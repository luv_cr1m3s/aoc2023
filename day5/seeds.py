#!/usr/bin/env python3

import re

def get_input(filename):
    result = ""

    with open(filename, "r") as file:
        result = file.read()

    return result

def gen_ranges(lims):
    ranges = []
    for i in lims:
        i = [ int(j) for j in i ]
        a, b, c = i
        d = 0
        tmp_range = [b, b + c - 1, a - b]
        ranges.append(tmp_range)
    
    return ranges


def proseeds(seeds, ranges):
    result = []

    for seed in seeds:
        flag = 0
        for r in ranges:
            if seed >= r[0] and seed <= r[1] and flag == 0:
                result.append(seed + r[2])
                flag = 1
        if flag == 0:
            result.append(seed)
            
    return result

def find_cell(alma):
    fields = alma.split(":") 
    seeds_flag = 0
    seeds = []
    ranges = None

    for i in fields:
        l = i.split("\n")
        l = [k for k in l if len(k) > 0]
        tmp_lim = []
        for n in l:
            j = re.findall('\d+', n)
            if len(j) > 0:
                if seeds_flag == 0:
                    seeds = [int(b) for b in j]
                    #seeds = [int(b) for b in j]
                    seeds_flag = 1
                    #print(seeds)
                    #print("======================")
                else:
                    tmp_lim.append(j)
        if len(tmp_lim) > 0:
            print(*tmp_lim)
            ranges = gen_ranges(tmp_lim)
            print(seeds)
            print(ranges)
            seeds = proseeds(seeds, ranges)
            print(seeds)
            print("==================")
    print("================")
    print(seeds)
    print(min(seeds))

if __name__ == '__main__':
    a = """
seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4
"""
    
    b = get_input("first_star")
    #print(a)
    find_cell(b)
