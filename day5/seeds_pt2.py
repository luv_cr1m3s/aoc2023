#!/usr/bin/env python3

import re

def get_input(filename):
    result = ""

    with open(filename, "r") as file:
        result = file.read()

    return result

def gen_ranges(lims):
    ranges = []
    for i in lims:
        i = [ int(j) for j in i ]
        a, b, c = i
        d = 0
        tmp_range = [b, b + c - 1, a - b]
        ranges.append(tmp_range)
    
    return ranges


def proseeds(seeds, ranges):
    result = []

    while seeds:
        s, e = seeds.pop()
        for a, b, c in ranges:
            os = max(s, b)
            oe = min(e, b + c)
            if os < oe :
                result.append((os - b + a, oe - b + a))
                if os > s:
                    seeds.append((s, os))
                if e > oe:
                    seeds.append((oe, e))
                break
        else:
            result.append((s, e))
            
    return min(result)[0]

def find_cell(alma):
    fields = alma.split(":") 
    seeds_flag = 0
    seeds = []
    ranges = None
    ranges_list = []

    for i in fields:
        l = i.split("\n")
        l = [k for k in l if len(k) > 0]
        tmp_lim = []
        for n in l:
            j = re.findall('\d+', n)
            if len(j) > 0:
                if seeds_flag == 0:
                    seeds_data = [int(b) for b in j]
                    start_s = seeds_data[::2]
                    start_l = seeds_data[1::2]
                    #print(start_s, start_l)
                    for (e, r) in zip(start_s, start_l):
                        seeds.append((e, r))
                    #seeds = [int(b) for b in j]
                    seeds_flag = 1
                    #print(seeds)
                    #print("======================")
                else:
                    tmp_lim.append(j)
        if len(tmp_lim) > 0:
            ranges = gen_ranges(tmp_lim)
            ranges_list.append(ranges)

    #print(seeds)
    #print("==========")
    #print(ranges_list)
    #print(seeds)
    print(proseeds(seeds, ranges))

if __name__ == '__main__':
    a = """
seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4
"""
    
    b = get_input("first_star")
    #print(a)
    find_cell(b)
