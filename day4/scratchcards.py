#!/usr/bin/env python3

import re


def get_input(filename):
    result = ""

    with open(filename, "r") as file:
        result = file.read()

    return result


def scratch_card(input):
    input_array = input.split("\n")
    input_array = [i.strip() for i in input_array]
    input_array = [i for i in input_array if len(i) > 0]

    result = 0

    for line in input_array:
        card_points = 0
        numbers = line.split(":")[1]
        winnning, regular = numbers.split("|")
        win_arr = winnning.split(" ")
        reg_arr = regular.split(" ")
        reg_arr = [i for i in reg_arr if len(i) > 0]

        for n in reg_arr:
            if n in win_arr:
                if card_points == 0:
                    card_points = 1
                else:
                    card_points *= 2
        result += card_points

    return result


if __name__ == '__main__':
    a = """
    Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
    Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
    Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
    Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
    Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
    Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
    """
    
    b = get_input("first_star")
    #print(a)
    print(scratch_card(b))
