#!/usr/bin/env python3

import re


def get_input(filename):
    result = ""

    with open(filename, "r") as file:
        result = file.read()

    return result

def count_win(win, reg):
    result = 0

    for r in reg:
        if r in win:
            result += 1
    
    return result


def count_cards(input):
    input_array = input.split("\n")
    input_array = [i.strip() for i in input_array]
    input_array = [i for i in input_array if len(i) > 0]

    cards_stack = {}
    result = 0

    for line in input_array:
        card, numbers = line.split(":")
        card_n = card.split(" ")
        card_n = [i for i in card_n if len(i) > 0 ]
        card_num = int(card_n[1])
        winnning, regular = numbers.split("|")
        win_arr = winnning.split(" ")
        reg_arr = regular.split(" ")
        reg_arr = [i for i in reg_arr if len(i) > 0]
        
        cards_stack[card_num] = {"win": win_arr, "reg": reg_arr, "num" : 1}

    for k, v in cards_stack.items():
        win_count = count_win(cards_stack[k]["win"], cards_stack[k]["reg"])
        print(win_count)
        
        for i in range(k+1, k + 1 + win_count):
            print(i)
            cards_stack[i]["num"] += 1 * cards_stack[k]["num"]
    
    for k in cards_stack.keys():
        result += cards_stack[k]["num"]
        print(f"{k}: {cards_stack[k]['num']}")


    return result


if __name__ == '__main__':
    a = """
    Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
    Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
    Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
    Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
    Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
    Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
    """
    
    b = get_input("first_star")
    #print(a)
    print(count_cards(b))
