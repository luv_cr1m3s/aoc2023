#!/usr/bin/env python3

from math import gcd

def get_input(filename):
    result = ""

    with open(filename, "r") as file:
        result = file.read()

    return result

def parse_input(str):
    pattern, directions = str.split("\n\n")

    directions = directions.split("\n")
    directions_dict = {}
    for d in directions:
        name, conn = d.split(" = ")
        a, b= conn[1:-1].split(", ")
        directions_dict[name]  = [a, b]

    print(pattern, directions_dict.items())
    return pattern, directions_dict

def check_end(arr):
    for i in arr:
        if i[-1] != "Z":
            return False
    return True

def find_path(start, data, pattern):
    current = start
    count = 0

    while current[-1] != "Z":
        count += 1
        current = data[current][0 if pattern[0] == "L" else 1]
        pattern = pattern[1:] + pattern[0]
    
    return count



def walk(pattern, data):
    count = 0
    current = []

    for i in data.keys():
        if i[-1] == "A":
            current.append(i) 
    solution = []
    for c in current:
        solution.append(find_path(c, data, pattern))
    
    print(solution)

    lcm = solution.pop()

    for num in solution:
        lcm = lcm * num // gcd(lcm, num)
    
    print(lcm)

if __name__ == '__main__':
    a = get_input("first_star.txt")
    # a = get_input("test3.txt")
    pattern, data = parse_input(a)
    walk(pattern, data)