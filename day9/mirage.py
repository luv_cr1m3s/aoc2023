#!/usr/bin/env python3


def get_input(filename):
    result = ""

    with open(filename, "r") as file:
        result = file.read()

    return result


def parse_input(str):
    directions = str.split("\n")
    directions = [i for i in directions if len(i) > 0]
    result = []
    for i in directions:
        tmp = i.split(" ")
        tmp = [int(j) for j in tmp if len(j) > 0]
        result.append(tmp)

    return result


def history(data):
    result = 0
    for d in data:
        tmp = d
        last_v = 0
        while len(tmp) > 0 and not all(x == 0 for x in tmp):
            print(tmp)
            last_v += tmp[-1]
            curr = []
            for i in range(len(tmp) - 1):
                curr.append(tmp[i+1] - tmp[i])
            tmp = curr

        print("====================")
        print(last_v)
        result += last_v
        print("====================")
    print(result)


if __name__ == '__main__':
    a = get_input("first_star.txt")
    # a = get_input("test.txt")
    print(a)
    arr = parse_input(a)
    for i in arr:
        print(i)
    print("====================")
    history(arr)
